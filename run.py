"""Файл запуска приложения"""
from os import environ
from web import app, socketio

PORT = int(environ.get('PORT', '5000'))
socketio.run(app, host='0.0.0.0', port=PORT)
