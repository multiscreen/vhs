FROM python:3.7.6-stretch

COPY . /app

WORKDIR /app

RUN pip install -r requirements.txt

CMD ["bash", "./run.sh"]
